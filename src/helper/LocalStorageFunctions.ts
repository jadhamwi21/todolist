import { TodoInterface } from "../actions/payloadInterfaces";

export const StoreTodo = (Todo: TodoInterface): void => {
	const isLocalStorageEmpty: boolean = localStorage.length === 0;
	if (isLocalStorageEmpty) {
		localStorage.setItem("1", JSON.stringify(Todo));
	} else {
		const HighestKeyValue: string =
			Object.keys(localStorage).sort()[localStorage.length - 1];
		const NewKeyValue: string = (parseInt(HighestKeyValue) + 1).toString();
		console.log(Object.keys(localStorage));
		localStorage.setItem(NewKeyValue, JSON.stringify(Todo));
	}
};
type TodoInLocalStorage = [string, string];
export const RemoveCompletedTodos = (): void => {
	const LS = localStorage;
	const { TodoEntries, KeyIndex, ValueIndex } = ((): {
		TodoEntries: TodoInLocalStorage[];
		KeyIndex: number;
		ValueIndex: number;
	} => {
		const TodoEntries: TodoInLocalStorage[] = Object.entries(LS);
		const KeyIndex = 0;
		const ValueIndex = 1;
		return {
			TodoEntries: TodoEntries,
			KeyIndex: KeyIndex,
			ValueIndex: ValueIndex,
		};
	})();

	for (
		let currentElementIndex: number = 0;
		currentElementIndex < TodoEntries.length;
		currentElementIndex++
	) {
		const { Completed }: TodoInterface = JSON.parse(
			TodoEntries[currentElementIndex][ValueIndex]
		);
		if (Completed) {
			LS.removeItem(TodoEntries[currentElementIndex][KeyIndex]);
		}
	}
};
export const MarkTodoInLocalStorageAsCompleted = (TodoValue: string): void => {
	const Entries: TodoInLocalStorage[] = Object.entries(localStorage);
	const Key = 0;
	const Value = 1;
	Entries.forEach((Entry) => {
		const ParsedTodo: TodoInterface = JSON.parse(Entry[Value]);
		if (ParsedTodo.TodoValue === TodoValue) {
			const ModifiedTodoObject: TodoInterface = {
				TodoValue: TodoValue,
				Completed: true,
			};
			localStorage.setItem(
				Entry[Key].toString(),
				JSON.stringify(ModifiedTodoObject)
			);
		}
	});
};
export const RemoveTodoFromLocalStorage = (TodoValue: string): void => {
	const Entries: TodoInLocalStorage[] = Object.entries(localStorage);
	const Key = 0;
	const Value = 1;
	Entries.forEach((Entry) => {
		const ParsedTodo: TodoInterface = JSON.parse(Entry[Value]);
		if (ParsedTodo.TodoValue === TodoValue) {
			localStorage.removeItem(Entry[Key].toString());
		}
	});
};
