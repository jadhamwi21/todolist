import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import thunk, { ThunkMiddleware } from "redux-thunk";
import { AppActions } from "../actions/actions";
import { AppState, RootReducer } from "../reducer/RootReducer";

export const store = createStore(
	RootReducer,
	applyMiddleware(thunk as ThunkMiddleware<AppState, AppActions>)
);
store.subscribe(() => console.log(store.getState()));
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector;
