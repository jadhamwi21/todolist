import { TodoActions, TodoActionTypes } from "../actions/actions";
import { TodoInterface } from "../actions/payloadInterfaces";

export interface TodoReducerStateType {
	TodoInputFieldValue: string;
	TodoList: TodoInterface[];
	isFetching: boolean;
	isFirstTodoAddedByUser: boolean;
}
export const TodoReducerDefaultState: TodoReducerStateType = {
	TodoInputFieldValue: "",
	TodoList: [],
	isFetching: true,
	isFirstTodoAddedByUser: false,
};
export const TodoReducer = (
	state = TodoReducerDefaultState,
	action: TodoActions
): TodoReducerStateType => {
	switch (action.type) {
		case TodoActionTypes.UPDATE_TODO_INPUT_FIELD:
			return { ...state, TodoInputFieldValue: action.fieldPayload };
		case TodoActionTypes.APPEND_TODO:
			return { ...state, TodoList: [...state.TodoList, action.todoPayload] };
		case TodoActionTypes.TURN_FETCHING_OFF:
			return { ...state, isFetching: false };
		case TodoActionTypes.MARK_COMPLETED:
			return { ...state, TodoList: action.updatedTodoList };
		case TodoActionTypes.REMOVE_TODO:
			return { ...state, TodoList: action.updatedTodoList };
		default:
			return state;
	}
};
