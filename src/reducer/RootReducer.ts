import { combineReducers } from "redux";
import { TodoReducer } from "./TodoReducer";

export const RootReducer = combineReducers({
	Todo: TodoReducer,
});
export type AppState = ReturnType<typeof RootReducer>;
