import React from "react";
import { Provider } from "react-redux";
import { store } from "../store/store";
import Header from "./Header/Header";
import Main from "./Main/Main";
const App: React.FC<{}> = (): React.ReactElement => {
	return (
		<Provider store={store}>
			<Header />
			<Main />
		</Provider>
	);
};

export default App;
