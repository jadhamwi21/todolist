import React from "react";
import Styled from "styled-components";
import { connect } from "react-redux";
import AddLogo from "../../assets/AddLogo.svg";
import { AppState } from "../../reducer/RootReducer";
import { AppDispatch } from "../../store/store";
import {
	AppendFlags,
	appendTodo,
	updateTodoInputField,
} from "../../actions/actionCreators";
import { TodoInterface } from "../../actions/payloadInterfaces";
interface Props {
	FieldValue: string;
	FieldHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
	AddTodoHandler: (e: React.FormEvent, TodoValue: string) => void;
}

const TodoInputField = ({
	FieldValue,
	FieldHandler,
	AddTodoHandler,
}: Props) => {
	return (
		<InputFieldWrapper>
			<Form onSubmit={(e) => AddTodoHandler(e, FieldValue)}>
				<InputField
					type="text"
					spellCheck="false"
					value={FieldValue}
					onChange={FieldHandler}
				/>
				<AddTodoButtonContainer type="submit">
					<div>
						<img src={AddLogo} alt="add-icon" />
					</div>
				</AddTodoButtonContainer>
			</Form>
		</InputFieldWrapper>
	);
};
const InputFieldWrapper = Styled.div`
	width:62%;
    height:1.75em;
    margin-top:10px !important;
    position:relative;
    border-radius:4px;
`;
const InputField = Styled.input`
height:100%;
width:100%;
background-color:#E7E4E4;
outline:none;
border:initial;
padding:0px;
padding-left:0.5em;
box-sizing:border-box;
border-radius:4px;
autocorrect:off;
`;
const AddTodoButtonContainer = Styled.button`
position:absolute;
right:0;
top:0;
height:2.1em;
width:30px;
border:initial;
background-color:#7674EA;
box-sizing:border-box;
padding:0px;
 border-radius:4px;
 & > div{
	 height:100%;
	 width:100%;
	 display:grid;
	 position:relative;
	 top:-2px;
	 place-items:center;
     &:hover{
         cursor:pointer;
     }
     &:active{
         opacity:0.6;
     }
 }
`;
const Form = Styled.form`
width:100%;
height:100%;
`;

interface ListStatePropsType {
	FieldValue: string;
}
interface ListDispatchPropsType {
	FieldHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
	AddTodoHandler: (e: React.FormEvent, TodoValue: string) => void;
}
const mapStateToProps = (state: AppState): ListStatePropsType => ({
	FieldValue: state.Todo.TodoInputFieldValue,
});
const mapDispatchToProps = (dispatch: AppDispatch): ListDispatchPropsType => ({
	FieldHandler: (e: React.ChangeEvent<HTMLInputElement>) => {
		dispatch(updateTodoInputField(e.target.value));
	},
	AddTodoHandler: async (e: React.FormEvent, TodoValue: string) => {
		e.preventDefault();
		if (TodoValue !== "") {
			const TodoInstance: TodoInterface = {
				TodoValue: TodoValue,
				Completed: false,
			};
			dispatch(appendTodo(TodoInstance, AppendFlags.BasicAppending));
			dispatch(updateTodoInputField(""));
		}
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoInputField);
