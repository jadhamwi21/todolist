import { AnimatePresence, motion } from "framer-motion";
import React from "react";
import { TodoInterface } from "../../actions/payloadInterfaces";
import TodoIndividualComponent from "./TodoIndividualComponent";

interface Props {
	Todos: TodoInterface[];
}
const animationStagesProp = {
	initial: {
		opacity: 0,
	},
	animate: {
		opacity: 1,
	},
	exit: {
		opacity: 0,
	},
	transition: {
		duration: 0.4,
	},
};
const TodosLister = ({ Todos }: Props) => {
	return (
		<>
			{Todos.map((Todo) => (
				<TodoIndividualComponent Todo={Todo} key={Todo.TodoValue} />
			))}
		</>
	);
};

export default TodosLister;
