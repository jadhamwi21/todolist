import React from "react";
import Styled from "styled-components";
import TodoInputField from "./TodoInputField";
import TodoInputLabel from "./TodoInputLabel";
import TodoListComponent from "./TodoListComponent";
const Main: React.FC<{}> = (): React.ReactElement => {
	return (
		<MainContainer>
			<MainWrapper>
				<TodoInputLabel />
				<TodoInputField />
				<TodoListComponent />
			</MainWrapper>
		</MainContainer>
	);
};
const MainContainer = Styled.main`
height:100vh;
width:100%;
`;
const MainWrapper = Styled.div`
width:30%;
height:fit-content;
margin:4em auto;
& > div{
	margin:0 auto;
}
`;
export default Main;
