import React from "react";
import Styled from "styled-components";
import { deleteTodo, markTodoAsCompleted } from "../../actions/actionCreators";
import { useAppDispatch } from "../../store/store";
import { Operations } from "./TodoIndividualComponent";

interface Props {
	src: string;
	OperationValue: Operations;
	TodoValue: string;
}
const OperationComponent = ({ src, OperationValue, TodoValue }: Props) => {
	const dispatch = useAppDispatch();
	const onClickHandler = () => {
		if (OperationValue === Operations.MarkCompletion) {
			dispatch(markTodoAsCompleted(TodoValue));
		} else {
			dispatch(deleteTodo(TodoValue));
		}
	};
	return (
		<OperationWrapper onClick={onClickHandler}>
			<OperationLogo src={src} />
		</OperationWrapper>
	);
};
const OperationWrapper = Styled.div`
height:fit-content;
width:fit-content;
background-color:transparent;
transition-property:background-color;
transition-duration:0.1s;
transition-timing-function:ease-in;
border-radius:4px;
&:hover{
    background-color:rgba(0,0,0,0.17);
}
`;
const OperationLogo = Styled.img`
height:1.75em;
width:1.75em;
`;
export default OperationComponent;
