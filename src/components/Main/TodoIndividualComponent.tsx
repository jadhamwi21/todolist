import React from "react";
import { TodoInterface } from "../../actions/payloadInterfaces";
import Styled from "styled-components";
import MarkCompletedLogo from "../../assets/CheckLogo.svg";
import OperationComponent from "./OperationComponent";
import CompletionLayer from "./CompletionLayer";

const DeleteLogo =
	"https://img.icons8.com/material-outlined/24/000000/trash--v2.png";
interface Props {
	Todo: TodoInterface;
}
export enum Operations {
	Delete,
	MarkCompletion,
}
const TodoIndividualComponent = ({ Todo }: Props) => {
	return (
		<TodoWrapper>
			<TodoFlexbox>
				<TodoValue>{Todo.TodoValue}</TodoValue>
				<TodoOperations>
					<OperationComponent
						src={MarkCompletedLogo}
						OperationValue={Operations.MarkCompletion}
						TodoValue={Todo.TodoValue}
					/>
					<OperationComponent
						src={DeleteLogo}
						OperationValue={Operations.Delete}
						TodoValue={Todo.TodoValue}
					/>
				</TodoOperations>
			</TodoFlexbox>
			<CompletionLayer isCompleted={Todo.Completed === true} />
		</TodoWrapper>
	);
};
const TodoWrapper = Styled.div`
width:100%;
height:fit-content;
min-height:3em;
border-radius:4px;
background-color:#7674EA;
display:grid;
place-items:center;
user-select:none;
position:relative;
margin:10px 0px;
&:first-child{
	margin-top:0px;
}
&:last-child{
	margin-bottom:0px;
}
`;
const TodoFlexbox = Styled.div`
width:90%;
height:fit-content;
display:flex;
flex-direction:row;
align-items:center;
justify-content:space-between;
`;
const TodoValue = Styled.p`
color:white;
height:fit-content;
width:60%;
`;
const TodoOperations = Styled.div`
display:flex;
flex-direction:row;
align-items:center;
justify-content:space-between;
width:35%;
`;
export default TodoIndividualComponent;
