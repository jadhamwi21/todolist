import React from "react";
import Styled from "styled-components";

const TodoInputLabel: React.FC<{}> = (): React.ReactElement => {
	return <TodoInputLabelWrapper>Type Your Todo Below</TodoInputLabelWrapper>;
};
const TodoInputLabelWrapper = Styled.div`
    color:#7674EA;
    font-weight:600;
    height:fit-content;
    width:fit-content;
    font-size:24px;
`;
export default TodoInputLabel;
