import React from "react";
import Styled from "styled-components";

interface Props {
	isCompleted: boolean;
}
const CompletionLayer = ({ isCompleted }: Props) => {
	return (
		<Layer
			onClick={(e: React.MouseEvent) => e.stopPropagation()}
			style={
				isCompleted ? { visibility: "visible", cursor: "not-allowed" } : {}
			}
		/>
	);
};
const Layer = Styled.div`
position:absolute;
height:100%;
width:100%;
background: rgba(0,0,0,0.4);
visibility:hidden;
transition-property:visibility;
transition-duration:0.25s;
`;

export default CompletionLayer;
