import React from "react";
import { connect } from "react-redux";
import Styled from "styled-components";
import { TodoInterface } from "../../actions/payloadInterfaces";
import { useTodoFromLocalStorage } from "../../hooks/useTodoFromLocalStorage";
import { AppState } from "../../reducer/RootReducer";
import TodosLister from "./TodosLister";
import { AnimatePresence, motion } from "framer-motion";
interface Props {
	TodosList: TodoInterface[];
	IsFetching: boolean;
}
const FetchTodosAnimationVariants = {
	start: {
		opacity: 0,
	},
	animate: {
		opacity: 1,
	},
	end: {
		opacity: 0,
	},
};
const TodoListComponent = ({ TodosList, IsFetching }: Props) => {
	useTodoFromLocalStorage();
	return (
		<TodoListContainer>
			<AnimatePresence exitBeforeEnter>
				<FetchTodoTransition
					variants={FetchTodosAnimationVariants}
					initial="start"
					animate="animate"
					exit="end"
					transition={{ duration: 0.8 }}
				>
					{IsFetching ? `Fetching...` : <TodosLister Todos={TodosList} />}
				</FetchTodoTransition>
			</AnimatePresence>
		</TodoListContainer>
	);
};
const TodoListContainer = Styled.div`
width:62%;
height:fit-content;
margin-top:2em!important;
display:flex;
flex-direction:column;
`;
const FetchTodoTransition = motion.div;
interface ListStatePropsTypes {
	TodosList: TodoInterface[];
	IsFetching: boolean;
}
const mapStateToProps = (state: AppState): ListStatePropsTypes => ({
	TodosList: state.Todo.TodoList,
	IsFetching: state.Todo.isFetching,
});
export default connect(mapStateToProps)(TodoListComponent);
