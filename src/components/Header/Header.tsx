import React, { ReactElement, FC } from "react";
import Styled from "styled-components";
const Header: FC<{}> = (): ReactElement => {
	return (
		<HeaderContainer>
			<HeaderTodoListTitle>Todo List</HeaderTodoListTitle>
		</HeaderContainer>
	);
};
const HeaderContainer = Styled.header`
    background-color:#7674EA;
    height:80px;
    display:grid;
    place-items:center;
    width:100%;
`;
const HeaderTodoListTitle = Styled.div`
    height:fit-content;
    width:fit-content;
    color:white;
    font-weight:400;
    font-size:40px;
    user-select:none;
`;
export default Header;
