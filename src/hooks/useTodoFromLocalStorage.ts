import { useEffect } from "react";
import { fetchTodosFromLocalStorage } from "../actions/actionCreators";
import { RemoveCompletedTodos } from "../helper/LocalStorageFunctions";
import { useAppDispatch } from "../store/store";

export const useTodoFromLocalStorage = (): void => {
	const dispatch = useAppDispatch();
	useEffect(() => {
		const FetchTodos = () => {
			RemoveCompletedTodos();
			dispatch(fetchTodosFromLocalStorage());
		};
		FetchTodos();
	}, []);
};
