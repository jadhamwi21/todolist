import {
	MarkTodoInLocalStorageAsCompleted,
	RemoveTodoFromLocalStorage,
	StoreTodo,
} from "../helper/LocalStorageFunctions";
import { AppState } from "../reducer/RootReducer";
import { AppDispatch, store } from "../store/store";
import {
	AppendTodoAction,
	MarkCompletedAction,
	RemoveTodoAction,
	TodoActions,
	TodoActionTypes,
	TurnFetchingOff,
	UpdateTodoInputFieldAction,
} from "./actions";
import { TodoInterface } from "./payloadInterfaces";

export const updateTodoInputField = (
	InputValue: string
): UpdateTodoInputFieldAction => ({
	type: TodoActionTypes.UPDATE_TODO_INPUT_FIELD,
	fieldPayload: InputValue,
});
export const turnFetchingOff = (): TurnFetchingOff => ({
	type: TodoActionTypes.TURN_FETCHING_OFF,
});
export enum AppendFlags {
	InitialAppending,
	BasicAppending,
}
export const appendTodo = (Todo: TodoInterface, Flag: AppendFlags) => {
	return (dispatch: AppDispatch) => {
		const {
			Todo: { TodoList },
		}: AppState = store.getState();
		let isTodoToBeAppendedADuplicate = false;
		TodoList.forEach((CurrentTodo: TodoInterface) => {
			if (
				CurrentTodo.TodoValue.toUpperCase() === Todo.TodoValue.toUpperCase()
			) {
				isTodoToBeAppendedADuplicate = true;
			}
		});
		if (isTodoToBeAppendedADuplicate) {
			return;
		}
		if (Flag === AppendFlags.BasicAppending) {
			StoreTodo(Todo);
		}
		const DispatchAppendTodo = (): void => {
			const AppendTodoAction: AppendTodoAction = {
				type: TodoActionTypes.APPEND_TODO,
				todoPayload: Todo,
			};
			dispatch(AppendTodoAction);
		};
		DispatchAppendTodo();
	};
};

export const fetchTodosFromLocalStorage = () => {
	return (dispatch: AppDispatch) => {
		const TodoKeysFromLocalStorage = Object.keys(localStorage)
			.map((key) => parseInt(key))
			.sort();
		TodoKeysFromLocalStorage.forEach((TodoKey) => {
			const TodoMatchingThisKey = localStorage.getItem(TodoKey.toString());
			console.log(TodoMatchingThisKey);
			const ParsedTodoFromLocalStorage: TodoInterface = TodoMatchingThisKey
				? JSON.parse(TodoMatchingThisKey)
				: null;
			dispatch(
				appendTodo(ParsedTodoFromLocalStorage, AppendFlags.InitialAppending)
			);
		});
		dispatch(turnFetchingOff());
	};
};
export const markTodoAsCompleted = (TodoValue: string) => {
	return (dispatch: AppDispatch, getState: typeof store.getState) => {
		MarkTodoInLocalStorageAsCompleted(TodoValue);
		const UpdatedTodosList: TodoInterface[] = [...getState().Todo.TodoList].map(
			(Todo) => {
				if (Todo.TodoValue === TodoValue) {
					const ModifiedTodo: TodoInterface = {
						TodoValue: Todo.TodoValue,
						Completed: true,
					};
					return ModifiedTodo;
				}
				return Todo;
			}
		);
		const MarkCompletedAction: MarkCompletedAction = {
			type: TodoActionTypes.MARK_COMPLETED,
			updatedTodoList: UpdatedTodosList,
		};
		dispatch(MarkCompletedAction);
	};
};
export const deleteTodo = (TodoValue: string) => {
	return (dispatch: AppDispatch, getState: typeof store.getState) => {
		const Todos: TodoInterface[] = [...getState().Todo.TodoList].filter(
			(Todo: TodoInterface) => Todo.TodoValue !== TodoValue
		);
		RemoveTodoFromLocalStorage(TodoValue);
		const deleteTodoAction: RemoveTodoAction = {
			type: TodoActionTypes.REMOVE_TODO,
			updatedTodoList: Todos,
		};
		dispatch(deleteTodoAction);
	};
};
