export interface TodoInterface {
	TodoValue: string;
	Completed: boolean;
}
