import { TodoInterface } from "./payloadInterfaces";

/* -- Todo's (Actions - Enums) */
export enum TodoActionTypes {
	UPDATE_TODO_INPUT_FIELD,
	APPEND_TODO,
	REMOVE_TODO,
	MARK_COMPLETED,
	FETCH_TODOS_FROM_LOCAL_STORAGE,
	TURN_FETCHING_OFF,
}
export interface UpdateTodoInputFieldAction {
	type: TodoActionTypes.UPDATE_TODO_INPUT_FIELD;
	fieldPayload: string;
}
export interface AppendTodoAction {
	type: TodoActionTypes.APPEND_TODO;
	todoPayload: TodoInterface;
}
export interface RemoveTodoAction {
	type: TodoActionTypes.REMOVE_TODO;
	updatedTodoList: TodoInterface[];
}
export interface MarkCompletedAction {
	type: TodoActionTypes.MARK_COMPLETED;
	updatedTodoList: TodoInterface[];
}
export interface TurnFetchingOff {
	type: TodoActionTypes.TURN_FETCHING_OFF;
}
export interface FetchTodosFromLocalStorageAction {
	type: TodoActionTypes.FETCH_TODOS_FROM_LOCAL_STORAGE;
}
export type TodoActions =
	| UpdateTodoInputFieldAction
	| AppendTodoAction
	| MarkCompletedAction
	| FetchTodosFromLocalStorageAction
	| TurnFetchingOff
	| RemoveTodoAction;

export type AppActions = TodoActions;
